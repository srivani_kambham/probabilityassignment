package com.probability;

import org.apache.commons.math3.fraction.Fraction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProbabilityTest {

    @Test
    public void shouldCheckProbabilityTest() {

        assertTrue(new Probability(new Fraction(1,7)).checkProbability());
    }


    @Test
    void shouldReturnCombinedProbabilityTest() {

        String actualvalue =new Probability(new Fraction(1, 8)).calculateCombinedProbability(new Probability(new Fraction(1, 4))).toString();
        assertEquals("1 / 32", actualvalue);
    }

    @Test
    void shouldReturnUnionProbabilityTest() {

        String actualvalue = new Probability(new Fraction(1, 8)).calculateUnionProbability(new Probability(new Fraction(1, 4))).toString();
        assertEquals("11 / 32", actualvalue);
    }

    @Test
    void shouldReturnXorProbabilityTest() {

        String actualvalue = new Probability(new Fraction(1, 8)).calculateXorProbability(new Probability(new Fraction(1, 4))).toString();
        assertEquals("5 / 16", actualvalue);
    }

    @Test
    void shouldReturnInverseProbability() {

        String actualvalue = new Probability(new Fraction(1, 8)).calculateInverseProbability().toString();
        assertEquals("7 / 8", actualvalue);
    }

    @Test
    void shouldReturnEqualProbabilityTest() {

        boolean actualvalue = new Probability(new Fraction(1, 8)).checkEqualProbability(new Probability(new Fraction(1, 4)));
        assertEquals(false, actualvalue);
    }

    @Test
    void shouldReturnBayesianProbabilityTest() {

        String actualvalue = new Probability(new Fraction(1, 8)).calculateBayesianProbability(new Probability(new Fraction(1, 4))).toString();
        assertEquals("3 / 16", actualvalue);
    }
}

