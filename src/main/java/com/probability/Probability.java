package com.probability;

import org.apache.commons.math3.fraction.Fraction;


public class Probability {

    Fraction event;

    Probability(Fraction event) {
        this.event = event;
    }

    boolean checkProbability()
    {
        return event.floatValue()>0 && event.floatValue()<1;
    }

    Fraction calculateCombinedProbability(Probability probability) {

        return event.multiply(probability.event);

    }

    Fraction calculateUnionProbability(Probability probability) {

        return event.add(probability.event).subtract(calculateCombinedProbability(probability));
    }

    Fraction calculateXorProbability(Probability probability) {

        Fraction fraction3 = calculateCombinedProbability(probability);
        return event.add(probability.event).subtract(fraction3.multiply(2));

    }

    Fraction calculateBayesianProbability(Probability probability) {
        Fraction divisionfraction = event.divide(probability.event);
        return event.add(probability.event).multiply(divisionfraction);

    }

    Fraction calculateInverseProbability() {
        return event.negate().add(1);
    }

    @Override
    public String toString() {
        return event.toString();
    }

    boolean checkEqualProbability(Probability probability) {

        return event.equals(probability.event);

    }



}
